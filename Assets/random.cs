﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class random : MonoBehaviour {
	string resultName = "AV_result_";
	int fileindex = 0;

	private string getResultName(){
		return resultName + fileindex + ".txt";
	}

	public class ImagePair{
		public int[] data;
		public int this[int index]
		{
			get{
				if((index >2) || (index < 0))
					return -1;
				else
					return data[index];
			}
			set{
				data[index] = value;
			}
		}

		public ImagePair(){
			data = new int[2];
		}
		public ImagePair(int i1, int i2){
			data = new int[2];
			data[0] = i1;
			data[1] = i2;
		}
	}

	// Use this for initialization
	void Start () {

		int loop = 27;

		List<ImagePair> testList = new List<ImagePair>();
		List<ImagePair> randList = new List<ImagePair>();

		for(int i = 0 ; i < loop ; ++i){
			for(int j = (i + 1) ; j < loop ; ++j){
				testList.Add(new ImagePair(i, j));
//				testList[index].data[0] = i;
//				testList[index].data[1] = j;
			}
		}

		Random.seed = (int)Time.time;
		int testCount = testList.Count;

		for(int i = testList.Count; i > 0 ; --i){
			int testIndex = (int)(Random.value * (float)testList.Count);
			randList.Add(testList[testIndex]);
//			Debug.Log(i + " " + testList[testIndex][0] + " " + testList[testIndex][1]);
			if(Random.value < 0.5){
				int temp = randList[testCount - i][0];
				randList[testCount - i][0] = randList[testCount - i][1];
				randList[testCount - i][1] = temp;
			}
			testList.RemoveAt(testIndex);
		}

		for(fileindex = 0; fileindex < int.MaxValue ; ++fileindex){
			if(File.Exists(getResultName())){
				continue;
			}
			else{
//				File.Create(getResultName());
				File.WriteAllText(getResultName(), "");
				break;
			}
		}

		if (fileindex == int.MaxValue) {
			Application.Quit();
		}
	}

	int updateCount = 0;
	// Update is called once per frame
	void Update () {
		if(updateCount < 10){
			File.AppendAllText(getResultName(),updateCount.ToString() + "\n");
		}
		++updateCount;
	}
}
