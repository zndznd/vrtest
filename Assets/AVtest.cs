﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
//using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class AVtest : MonoBehaviour {
	public GameObject leftImage;
	public GameObject rightImage;

	private int imageIndex = 0;
	private int testIndex = 0;
	List<ImagePair> testList = new List<ImagePair>();
	List<ImagePair> randList = new List<ImagePair>();

	//result
	public int fileCount;
	private int fileindex = 0;
	private string resultName = "AV_result_";
	private string getResultName(){
		return ("/sdcard/AVresult/" + resultName + fileindex.ToString() + ".txt");
//		return ("/sdcard/" + resultName + fileindex.ToString() + ".txt");
//		return ("/data/data" + "/" + resultName + fileindex.ToString() + ".txt");
	}

	//ui
	public Text leftText;
	public Text rightText;
	public Canvas leftCanvas;
	public Canvas rightCanvas;
	public float showTime;
	public float waitTime;

	//text
	private string startText = "테스트를 시작하려면 B버튼을 눌러주세요";
//	private string resultText = "결과 바꾸기 : Y.\n확인 : B";
//	private string reshowText = "다시 보기 : A";

	public class ImagePair{
		public int[] data;
		public int this[int index]
		{
			get{
				if((index >2) || (index < 0))
					return -1;
				else
					return data[index];
			}
			set{
				data[index] = value;
			}
		}
		
		public ImagePair(){
			data = new int[2];
		}
		public ImagePair(int i1, int i2){
			data = new int[2];
			data[0] = i1;
			data[1] = i2;
		}
	}

	void changeImage(int num){
		if (num == 0) {
			leftImage.renderer.material.mainTexture = null;
			rightImage.renderer.material.mainTexture = null;
		}
		leftCanvas.scaleFactor = 0.0f;
		rightCanvas.scaleFactor = 0.0f;
//
//		leftCanvas.planeDistance = 100;
//		rightCanvas.planeDistance = 100;
//
		Texture tex= Resources.Load ("stereo/left/" + num) as Texture;
		leftImage.renderer.material.mainTexture = tex;
		
		tex= Resources.Load("stereo/right/" + num) as Texture;
		rightImage.renderer.material.mainTexture = tex;
	}

	void setText(string text){
		leftText.text = text;
		rightText.text = text;

		
		leftCanvas.scaleFactor = 1.0f;
		rightCanvas.scaleFactor = 1.0f;
//		leftCanvas.planeDistance = 1;
//		rightCanvas.planeDistance = 1;
	}

	// Use this for initialization
	void Start () {		
		for(fileindex = 0; fileindex < int.MaxValue ; ++fileindex){
			if(File.Exists(getResultName())){
				continue;
			}
			else{
				File.Create(getResultName());
				break;
			}
		}
		
		//		DirectoryInfo dInfo = new DirectoryInfo("Assets/Resources/stereo/left/");
		//		fileCount = dInfo.GetFiles("*.png").GetLength(0);
		//generate test set
		for(int i = 1 ; i < (fileCount + 1) ; ++i){
			for(int j = (i + 1) ; j < (fileCount + 1) ; ++j){
				testList.Add(new ImagePair(i, j));
			}
		}
		
		Random.seed = fileindex;
		int testCount = testList.Count;
		
		for(int i = testList.Count; i > 0 ; --i){
			int index = (int)(Random.value * (float)testList.Count);
			randList.Add(testList[index]);
//			Debug.Log(i + " " + randList[testCount - i][0] + " " + randList[testCount - i][1]);
			if(Random.value < 0.5){
				int temp = randList[testCount - i][0];
				randList[testCount - i][0] = randList[testCount - i][1];
				randList[testCount - i][1] = temp;
			}
		}
		testList.Clear();

		if (fileindex == int.MaxValue) {
			Application.Quit();
		}

//		setText (getResultName());
//		changeImage (randList[testIndex][imageIndex]);
		changeImage (0);
		setText (startText);

		Text[] texts = leftText.GetComponentsInChildren<Text> ();
		foreach (Text t in texts) {
			if(t != leftText)
				t.enabled = false;
		}
		texts = rightText.GetComponentsInChildren<Text> ();
		foreach (Text t in texts) {
			if(t != rightText)
				t.enabled = false;
		}
	}

	private bool checkArousal = false;
	private int arousalIndex = 0;
	private bool checkValance = false;
	private int valanceIndex = 0;
	private string[] informText = {"첫 번째 이미지", "두 번째 이미지"};
	float delta = 0.0f;
	bool isStart = false;

	// Update is called once per frame
	void Update () {
		if (!isStart) {
			if (Input.GetButtonUp ("Fire2")) {
				isStart = true;
				changeImage(randList[testIndex][imageIndex]);
				
				Text[] texts = leftText.GetComponentsInChildren<Text> ();
				foreach (Text t in texts) {
					t.enabled = true;
				}
				texts = rightText.GetComponentsInChildren<Text> ();
				foreach (Text t in texts) {
					t.enabled = true;
				}
			}
			return;
		}

		if(testIndex > randList.Count){
			Application.Quit();
		}

//		check time;
		delta += Time.deltaTime;

		if((imageIndex == 0) && (delta > showTime)){
			imageIndex = 1;
			delta = 0.0f;
			changeImage(randList[testIndex][imageIndex]);
		}
		else if((imageIndex == 1) && (delta > showTime)){
			imageIndex = 2;
		}
		//compare two image
		else if(imageIndex == 2){
			//compare arousal
			if(!checkArousal){
//				if (Input.GetButtonUp ("RTrigger")) {
//					arousalIndex = 1;
//				}
//				else if (Input.GetButtonUp ("LTrigger")) {
//					arousalIndex = 0;
//				}
				if (Input.GetButtonUp ("Jump")) {
					if(arousalIndex == 0)
						arousalIndex = 1;
					else
						arousalIndex = 0;
				}
				setText("Arousal : " + informText[arousalIndex]);

				if(Input.GetButtonUp("Fire2")){
					checkArousal = true;
				}
				if(Input.GetButtonUp("Fire1")){
					imageIndex = 0;
					checkArousal = false;
					arousalIndex = 0;
					checkValance = false;
					valanceIndex = 0;
					delta = 0.0f;
					changeImage(randList[testIndex][imageIndex]);
				}
			}
			//compare valance
			else if(!checkValance){
//				if (Input.GetButtonUp ("RTrigger")) {
//					valanceIndex = 1;
//				}
//				else if (Input.GetButtonUp ("LTrigger")) {
//					valanceIndex = 0;
//				}
				if (Input.GetButtonUp ("Jump")) {
					if(valanceIndex == 0)
						valanceIndex = 1;
					else
						valanceIndex = 0;
				}

				setText("Valance : " + informText[valanceIndex]);

				if(Input.GetButtonUp("Fire2")){
					checkValance = true;
				}
				if(Input.GetButtonUp("Fire1")){
					imageIndex = 0;
					checkArousal = false;
					arousalIndex = 0;
					checkValance = false;
					valanceIndex = 0;
					delta = 0.0f;
					changeImage(randList[testIndex][imageIndex]);
				}
				delta = 0.0f;
			}

			else if(delta < waitTime){
				setText ("다음 테스트로 넘어갑니다.");
//				setText ("waitting");
			}

			else{
				imageIndex = 0;
				++testIndex;
				changeImage(randList[testIndex][imageIndex]);
				File.AppendAllText(getResultName(), randList[testIndex][0] + " " + randList[testIndex][1]  + " " + arousalIndex + " " + valanceIndex + "\n");
				delta = 0.0f;
				checkArousal = false;
				arousalIndex = 0;
				checkValance = false;
				valanceIndex = 0;
			}
		}
//		// compare arou	sal
//		if (Input.GetButtonUp ("Jump")) {
//			arousalIndex = 1;
//			if(!checkArousal){
//				checkArousal = true;
//			}
//		}
//		else if (Input.GetButtonUp ("Fire3")) {
//			arousalIndex = 2;
//			if(!checkArousal){
//				checkArousal = true;
//			}
//		}
//
//		// compare pleasure
//		if (Input.GetButtonUp ("Fire2")) {
//			pleasureIndex = 1;
//			if(!checkPleasure){
//				checkPleasure = true;
//			}
//		}
//
//		else if (Input.GetButtonUp ("Fire1")) {
//			pleasureIndex = 2;
//			if(!checkPleasure){
//				checkPleasure = true;
//			}
//		}
//
//		if(Input.GetButtonUp("Submit") && checkPleasure && checkArousal){
//			if(testIndex >= randList.Count){
//				Application.Quit();
//			}
//			File.AppendAllText(getResultName(), randList[testIndex][0] + " " + randList[testIndex][1]  + " " + arousalIndex + " " + pleasureIndex + "\n");
//
//			++testIndex;
//			arousalIndex = 0;
//			pleasureIndex = 0;
//			checkArousal = false;
//			checkPleasure = false;
//			imageIndex = 0;
//			changeImage(randList[testIndex][0]);
//		}
//
//		//change image
//		if (Input.GetButtonUp ("RTrigger")) {
//			imageIndex = 1;
//			changeImage(randList[testIndex][imageIndex]);
//		}
//
//		if (Input.GetButtonUp ("LTrigger")) {
//			imageIndex = 0;
//			changeImage(randList[testIndex][imageIndex]);
//		}
//
//		setText ("Test" + testIndex + " " + ", Image " + (imageIndex + 1 ) + ", Arosual : " + arousalIndex + " Pleasure : " + pleasureIndex);
	}
}
