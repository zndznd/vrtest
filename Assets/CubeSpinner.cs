﻿using UnityEngine;
using System.Collections;

public class CubeSpinner : MonoBehaviour {

	private static float SPIN_SPEED = 100f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate() {
		transform.Rotate (new Vector3(0, Time.deltaTime * SPIN_SPEED, 0));
	}
}
